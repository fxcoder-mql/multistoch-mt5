# Список изменений в MultiStoch

## 9.0 (2023-08-01)

- только MT5
- исправлено: остаются старые бары за пределами `MaxBars` (fxcoder-mql/multistoch-mt5#2)

## 8.2 (2020-01-15)

- исправлено: не работают параметры цвета в MT4 (fxcoder-mql/multistoch#62)

## 8.1 (2020-01-13)

- исправлено: неверная работа в MT5 (fxcoder-mql/multistoch#60)

## 8.0 (2019-03-31)

- `ExpPeriodStep` -> `PeriodStepType`, `ExpSlowingStep` -> `SlowingStepType`: выбор из нескольких типов шагов периода
- новые параметры отображения: `Theme` (цветовая тема), `GradientType` (тип градиента), `MidColor` (средний цвет)
- новый параметр Slowings для указания периодов `Slowing` вручную
- при неверных входных параметрах индикатор теперь не отваливается, а лишь сообщает об ошибке (в заголовке)
- удалён параметр `SlowingMethod`, т.к. он не влияет на расчёты

## 7.1 (2016-02-04)

- изменены ссылки на сайт
- список изменений в коде

## 7.0 (2016-01-31)

- вариант для MT5
- тип параметра `SlowingMethod` изменён на `ENUM_MA_METHOD`

## 6.0 (2015-12-30)

- из имени файла убран префикс `+`
- изменены ссылки на сайт
- оптимизация
- изменены отображаемые названия параметров
- добавлен параметр Price type (тип цены для расчёта: `Low/High` или `Close/Close`)

## 5.2 (2014-02-15)

- поддержка MT4 build 600+
