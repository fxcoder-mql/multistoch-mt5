/*
Copyright 2023 FXcoder

This file is part of MultiStoch.

MultiStoch is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MultiStoch is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with MultiStoch. If not, see
http://www.gnu.org/licenses/.
*/

#property copyright "MultiStoch 9.0. © FXcoder"
#property link      "https://fxcoder.blogspot.com"


#define MAX_PERIOD 10000
#define MAX_PLOTS 11

#property indicator_separate_window
#property indicator_buffers MAX_PLOTS
#property indicator_plots MAX_PLOTS

#property indicator_color1 clrDodgerBlue
#property indicator_color2 C'62,123,218'
#property indicator_color3 C'94,102,182'
#property indicator_color4 C'126,82,145'
#property indicator_color5 C'158,61,109'
#property indicator_color6 C'190,41,72'
#property indicator_color7 C'222,20,36'
#property indicator_color8 clrRed

#property indicator_minimum 0
#property indicator_maximum 100


#include "MultiStoch-include/enum/gradient.mqh"
#include "MultiStoch-include/enum/step.mqh"
#include "MultiStoch-include/enum/multi_theme.mqh"
#include "MultiStoch-include/plot/gradientlineplotset.mqh"
#include "MultiStoch-include/conv.mqh"
#include "MultiStoch-include/ind.mqh"
#include "MultiStoch-include/indicator.mqh"
#include "MultiStoch-include/multi.mqh"
#include "MultiStoch-include/series.mqh"
#include "MultiStoch-include/symbol.mqh"


input int               LineCount = 8;                // Line Count

input group "K-PERIODS"
input int               PeriodStart = 3;              // K-Period Start
input int               PeriodEnd = 89;               // K-Period End
input ENUM_STEP         PeriodStepType = STEP_EXP;    // K-Period Step
input string            Periods = "";                 // K-Period Custom (overrides above params)

input group "SLOWINGS"
input int               SlowingStart = 1;             // Slowing Start
input int               SlowingEnd = 5;               // Slowing End
input ENUM_STEP         SlowingStepType = STEP_EXP;   // Slowing Step
input string            Slowings = "";                // Slowing Custom (overrides above params)

input group "SOURCE"
input string            Sym = "";                     // Symbol
input bool              Reverse = false;              // Reverse
input ENUM_STO_PRICE    PriceType = STO_LOWHIGH;      // Price Type

input group "VISUAL"
input ENUM_MULTI_THEME  Theme = MULTI_THEME_RED_DDGBLUE;   // Theme
input ENUM_GRADIENT     GradientType = GRADIENT_RGB;       // Gradient Type
input color             StartColor = Red;                  // Start Color
input color             MidColor = clrNONE;                // Middle Color (None = do not use)
input color             EndColor = clrDodgerBlue;          // End Color
input bool              StartInFront = true;               // Start in Front
input int               LineWidth = 1;                     // Line Width (0 = do not set)

input group "ETC"
input int               Shift = 0;          // Shift
input int               MaxBars = 5000;     // Maximum Number of Bars


int periods_[], slowings_[]; // наборы периодов
bool is_chart_symbol_; // является ли расчетный символ символом чарта

int period_count_, period_start_, period_end_, slowing_start_, slowing_end_;
string sym_name_;
CSymbol sym_(Sym);
CSeries ser_(Sym, PERIOD_CURRENT);
int periods_draw_begin_;

CGradientLinePlotSet plots_;
int handles_[];
bool is_initialized_ = false;


void OnInit()
{
	is_initialized_ = init_source() && init_periods() && init_plots() && init_indicators(); // keep order

	_indicator.digits(2);

	if (!is_initialized_)
	{
		_indicator.short_name(_mql.program_name() + ": Wrong parameters.");
		return;
	}

	_indicator.short_name(_mql.program_name() + ": " +
		(Reverse ? "-" : "") + sym_name_ +
		"(" +
			_conv.range_to_string(periods_, enum_step_to_string(PeriodStepType, true)) +
			"," +
			_conv.range_to_string(slowings_, enum_step_to_string(SlowingStepType, true)) +
		")");
}

int OnCalculate(const int rates_total, const int prev_calculated, const datetime &time[], const double &open[], const double &high[], const double &low[], const double &close[], const long &tick_volume[], const long &volume[], const int &spread[])
{
	if (!is_initialized_)
		return 0;

	ArraySetAsSeries(time, true);

	int bars_to_calc = _ind.get_bars_to_calculate(rates_total, prev_calculated, is_chart_symbol_ ? 1 : 5, MaxBars, periods_draw_begin_);
	int calculated_bars = rates_total - bars_to_calc;

	plots_.empty(prev_calculated == 0 ? -1 : bars_to_calc);

	// rates are not loaded
	if (rates_total != plots_.size())
	{
		return 0;
	}

	for (int i = bars_to_calc - 1; i >= 0; i--)
	{
		int pbar = rates_total - 1 - i;
		double stoch = EMPTY_VALUE;

		for (int p = 0; p < period_count_; p++)
		{
			stoch = _ind.get_value(handles_[p], 0, time[i]);
			if (stoch == EMPTY_VALUE)
				break;

			plots_[p].buffer[pbar] = Reverse ? (100.0 - stoch) : stoch;
		}

		if (stoch == EMPTY_VALUE)
		{
			plots_.empty_bar(pbar);
			break;
		}

		calculated_bars++;
	}

	int draw_begin = MaxBars > 0 ? (rates_total - MaxBars) : (rates_total - calculated_bars);
	plots_.draw_begin(_math.max(draw_begin, periods_draw_begin_));

	return calculated_bars;
}


void OnDeinit(const int reason)
{
	_ind.release(handles_);
}

bool init_source()
{
	sym_name_ = sym_.name();
	is_chart_symbol_ = sym_name_ == _Symbol;
	return sym_.exists();
}

bool init_periods()
{
	period_count_ = _math.clamp(LineCount, 1, MAX_PLOTS);

	period_start_ = _math.clamp(PeriodStart, 1, MAX_PERIOD);
	period_end_ = _math.clamp(PeriodEnd, 1, MAX_PERIOD);

	slowing_start_ = _math.clamp(SlowingStart, 1, MAX_PERIOD);
	slowing_end_ = _math.clamp(SlowingEnd, 1, MAX_PERIOD);

	// Расчет периодов
	period_count_ = _multi.periods(period_start_, period_end_, period_count_, PeriodStepType, Periods, periods_);
	if (period_count_ <= 0)
		return false;

	int nslowings = _multi.periods(slowing_start_, slowing_end_, period_count_, SlowingStepType, Slowings, slowings_);
	if (period_count_ != nslowings)
	{
		if (nslowings != 1)
			return false;

		// Если указан только один период, считать его одинаковым для всех
		int slowing = slowings_[0];
		ArrayResize(slowings_, period_count_);
		ArrayInitialize(slowings_, slowing);
	}

	if (StartInFront)
	{
		ArrayReverse(periods_);
		ArrayReverse(slowings_);
	}

	return true;
}

bool init_plots()
{
	if (!plots_.init(0, 0, period_count_, Theme, StartColor, MidColor, EndColor, GradientType, StartInFront).is_initialized())
		return false;

	plots_.shift(Shift).line_width(LineWidth > 0 ? LineWidth : -1);//.show_data(false);
	periods_draw_begin_ = _math.max(periods_) + _math.max(slowings_);
	plots_.draw_begin(periods_draw_begin_).show_data(false);
	plots_.disable_unused(MAX_PLOTS);
	return true;
}

bool init_indicators()
{
	ArrayResize(handles_, period_count_);

	// в 5 без выбора символа в обзоре не рабоатет, выбрать
	sym_.select(true);

	for (int p = 0; p < period_count_; p++)
	{
		int handle = iStochastic(sym_name_, _Period, periods_[p], 1, slowings_[p], MODE_SMA, PriceType);
		if (handle == INVALID_HANDLE)
			return false;

		handles_[p] = handle;
	}

	return true;
}
