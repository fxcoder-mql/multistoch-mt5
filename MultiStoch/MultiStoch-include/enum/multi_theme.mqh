/*
Copyright 2023 FXcoder

This file is part of MultiStoch.

MultiStoch is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MultiStoch is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with MultiStoch. If not, see
http://www.gnu.org/licenses/.
*/

// Цветовая схема (тема) для градиентных индикаторов. © FXcoder

// При добавлении новой темы не использовать удалённый номер. Перенумерацию не делать.
// Сортировать по алфавиту. "Custom..." всегда первая, специальные - последние.
enum ENUM_MULTI_THEME
{
	MULTI_THEME_CUSTOM                              = 0,      // Custom...

	MULTI_THEME_DPPINK_DDGBLUE_LTGREEN              = 19,     // Deep Pink - Dodger Blue - Light Green
	MULTI_THEME_GOLD_DKGREEN                        = 20,     // Gold - Dark Green
	MULTI_THEME_GOLD_DKGREEN_RYBLUE                 = 12,     // Gold - Dark Green - Royal Blue *
	MULTI_THEME_GOLD_ORANGERED_PURPLE               = 1,      // Gold - Orange Red - Purple
	MULTI_THEME_GOLD_BLUE                           = 5,      // Gold - Blue
	MULTI_THEME_GRAY_STBLUE                         = 11,     // Gray - Steel Blue
	MULTI_THEME_GRYELLOW_MIDNBLUE_MVRED             = 7,      // Green Yellow - Midnight Blue - Medium Violet Red
	MULTI_THEME_HPINK_LSKBLUE                       = 16,     // Hot Pink - Light Sky Blue
	MULTI_THEME_LIME_BLUE_MAGENTA                   = 3,      // Lime - Blue - Magenta
	MULTI_THEME_MAGENTA_STBLUE_FGREEN               = 8,      // Magenta - Steel Blue - Forest Green
	MULTI_THEME_MVRED_DKSLBLUE_INDIGO               = 22,     // Medium Violet Red - Dark Slate Blue - Indigo
	MULTI_THEME_PINK_PDBLUE                         = 15,     // Pink - Powder Blue
	MULTI_THEME_RED_SPRGREEN_MAGENTA                = 21,     // Red - Spring Green - Magenta (HSV Rainbow)
	MULTI_THEME_RED_LMGREEN_BLUE                    = 10,     // Red - Lime Green - Blue
	MULTI_THEME_RED_DDGBLUE                         = 14,     // Red - Dodger Blue
	MULTI_THEME_RED_SILVER_BLUE                     = 9,      // Red - Silver - Blue
	MULTI_THEME_TOMATO_PDBLUE_FGREEN                = 13,     // Tomato - Powder Blue - Forest Green *
	MULTI_THEME_WHITE_BLACK                         = 24,     // White - Black *
	MULTI_THEME_YELLOW_ORANGERED_DSBLUE             = 26,     // Yellow - Orange Red - Deep Sky Blue *
	MULTI_THEME_YELLOW_RED_LIME                     = 2,      // Yellow - Red - Lime
	MULTI_THEME_YELLOW_SKBLUE_CRIMSON               = 17,     // Yellow - Sky Blue - Crimson

	MULTI_THEME_START_BGAVG25                       = 1010,   // Custom start - 25% mix with background *
	MULTI_THEME_START_INVERSE                       = 1007,   // Custom start - inverted
	MULTI_THEME_START_SV_INVERSE                    = 1008,   // Custom start - inverted (SV only)
	MULTI_THEME_FG_BG                               = 1001,   // Text - Background
};

// Вернуть false, если тема не основана на фиксированных цветах, и требуется дополнительная обработка.
bool enum_multi_theme_to_colors(const ENUM_MULTI_THEME theme, color &start_color, color &mid_color, color &end_color)
{
	if (theme >= 1000)
		return false;

	if (theme == MULTI_THEME_CUSTOM)                          // 0
	{
		// ничего не делать, цвета уже заданы
	}
	else if (theme == MULTI_THEME_GOLD_ORANGERED_PURPLE)      // 1
	{
		start_color = clrGold;
		mid_color = clrOrangeRed;
		end_color = clrPurple;
	}
	else if (theme == MULTI_THEME_YELLOW_RED_LIME)            // 2
	{
		start_color = clrYellow;
		mid_color = clrRed;
		end_color = clrLime;
	}
	else if (theme == MULTI_THEME_LIME_BLUE_MAGENTA)          // 3
	{
		start_color = clrLime;
		mid_color = clrBlue;
		end_color = clrMagenta;
	}
	else if (theme == MULTI_THEME_GOLD_BLUE)                  // 5
	{
		start_color = clrGold;
		mid_color = clrNONE;
		end_color = clrBlue;
	}
	else if (theme == MULTI_THEME_GRYELLOW_MIDNBLUE_MVRED)    // 7
	{
		start_color = clrGreenYellow;
		mid_color = clrMidnightBlue;
		end_color = clrMediumVioletRed;
	}
	else if (theme == MULTI_THEME_MAGENTA_STBLUE_FGREEN)      // 8
	{
		start_color = clrMagenta;
		mid_color = clrSteelBlue;
		end_color = clrForestGreen;
	}
	else if (theme == MULTI_THEME_RED_SILVER_BLUE)            // 9
	{
		start_color = clrRed;
		mid_color = clrSilver;
		end_color = clrBlue;
	}
	else if (theme == MULTI_THEME_RED_LMGREEN_BLUE)           // 10
	{
		start_color = clrRed;
		mid_color = clrLimeGreen;
		end_color = clrBlue;
	}
	else if (theme == MULTI_THEME_GRAY_STBLUE)                // 11
	{
		start_color = clrGray;
		mid_color = clrNONE;
		end_color = clrSteelBlue;
	}
	else if (theme == MULTI_THEME_GOLD_DKGREEN_RYBLUE)        // 12
	{
		start_color = clrGold;
		mid_color = clrDarkGreen;
		end_color = clrRoyalBlue;
	}
	else if (theme == MULTI_THEME_TOMATO_PDBLUE_FGREEN)       // 13
	{
		start_color = clrTomato;
		mid_color = clrPowderBlue;
		end_color = clrForestGreen;
	}
	else if (theme == MULTI_THEME_RED_DDGBLUE)                // 14
	{
		start_color = clrRed;
		mid_color = clrNONE;
		end_color = clrDodgerBlue;
	}
	else if (theme == MULTI_THEME_PINK_PDBLUE)                // 15
	{
		start_color = clrPink;
		mid_color = clrNONE;
		end_color = clrPowderBlue;
	}
	else if (theme == MULTI_THEME_HPINK_LSKBLUE)              // 16
	{
		start_color = clrHotPink;
		mid_color = clrNONE;
		end_color = clrLightSkyBlue;
	}
	else if (theme == MULTI_THEME_YELLOW_SKBLUE_CRIMSON)      // 17
	{
		start_color = clrYellow;
		mid_color = clrSkyBlue;
		end_color = clrCrimson;
	}
	else if (theme == MULTI_THEME_DPPINK_DDGBLUE_LTGREEN)     // 19
	{
		start_color = clrDeepPink;
		mid_color = clrDodgerBlue;
		end_color = clrLightGreen;
	}
	else if (theme == MULTI_THEME_GOLD_DKGREEN)               // 20
	{
		start_color = clrGold;
		mid_color = clrNONE;
		end_color = clrDarkGreen;
	}
	else if (theme == MULTI_THEME_RED_SPRGREEN_MAGENTA)       // 21
	{
		start_color = clrRed;
		mid_color = clrSpringGreen;
		end_color = clrMagenta;
	}
	else if (theme == MULTI_THEME_MVRED_DKSLBLUE_INDIGO)      // 22
	{
		start_color = clrMediumVioletRed;
		mid_color = clrDarkSlateBlue;
		end_color = clrIndigo;
	}
	else if (theme == MULTI_THEME_WHITE_BLACK)                // 24
	{
		start_color = clrWhite;
		mid_color = clrNONE;
		end_color = clrBlack;
	}
	else if (theme == MULTI_THEME_YELLOW_ORANGERED_DSBLUE)    // 26
	{
		start_color = clrYellow;
		mid_color = clrOrangeRed;
		end_color = clrDeepSkyBlue;
	}
	else
	{
		start_color = clrNONE;
		mid_color = clrNONE;
		end_color = clrNONE;
	}

	return true;
}
