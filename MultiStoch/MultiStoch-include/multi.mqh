/*
Copyright 2023 FXcoder

This file is part of MultiStoch.

MultiStoch is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MultiStoch is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with MultiStoch. If not, see
http://www.gnu.org/licenses/.
*/

// Функции для унификации мультипериодных индикаторов. © FXcoder

#include "enum/step.mqh"
#include "math.mqh"
#include "str.mqh"

class CMultiPeriodUtil
{
public:

	// Инициализировать набор периодов, заданный вручную. Вернуть количество периодов и их набор, не более max_line_count.
	static int periods_user(string user_periods, int max_line_count, int &periods[])
	{
		string parts[];
		const int period_count = _str.split_input_csv(user_periods, false, parts);

		int real_count = 0;
		ArrayResize(periods, period_count);

		for (int i = 0; i < period_count; i++)
		{
			periods[real_count++] = (int)StringToInteger(parts[i]);

			// жесткое ограничение на количество периодов
			if (real_count == max_line_count)
				break;
		}

		return ArrayResize(periods, real_count);
	}

	// no error handling
	static int range_period_linear(int first, int last, double position)
	{
		const int range = last - first;
		return _math.round_to_int(first + position * range);
	}

	// Расчет периодов для индикаторов серии Multi*
	static int range_periods_linear(int first, int last, int count, int &periods[])
	{
		ArrayResize(periods, count);

		if (count == 1)
		{
			periods[0] = first;
			return count;
		}

		// [0..count-1] => [0..1]
		const double step = 1.0 / (count - 1);
		for (int j = 0; j < count; j++)
			periods[j] = range_period_linear(first, last, j * step);

		return count;
	}

	// расчет периода для экспоненциально изменяющегося набора периодов по крайним значениям и позиции между ними (0..1)
	static int range_period_exponential(int first, int last, double position)
	{
		if (first == 0)
			return -1;

		/*
		p = a * b^x

		| first = a * b^0  =>  | first = a      =>  | a = first
		| last  = a * b^1  =>  | last  = a * b  =>  | b = last / first

		=> p = first * (last / first) ^ x
		*/

		const double a = first;
		const double b = last / first;
		return _math.round_to_int(a * pow(b, position));
	}

	// Расчет периодов для индикаторов серии Multi*
	static int range_periods_exponential(int first, int last, int count, int &periods[])
	{
		ArrayResize(periods, count);

		if (count == 1)
		{
			periods[0] = first;
			return count;
		}

		const double step = 1.0 / (count - 1);
		for (int i = 0; i < count; i++)
			periods[i] = range_period_exponential(first, last, i * step);

		return count;
	}

	static int range_period_parabolic(int first, int last, double position)
	{
		/*
		Расчёты довольно запутанные, и я не уверен, что они имеют хоть какой-то смысл,
		  но на практике такой вариант бывает интересным. В формулах x - это position.

		p = a * (b + x)^2

		| first = a * (b + 0)^2  =>  | a = first / b^2
		| last  = a * (b + 1)^2  =>  | last/first  = (b + 1)^2 / b^2

		...=>...

		k = sqrt(p2/p1) - (±1)
		p = first * (kx ± 1)^2

		=>

		s = ±1
		k = sqrt(p2/p1) - s
		p = first * (kx + s)^2

		+ доп. условие (полностью выдуманный критерий, на практике может быть интересен и другой вариант):
		p(first=1, last=4, x=2) = 9

		((sqrt(4/1) + s) * 2 + s)^2 = 9  =>  (2*(2-s)+s)^2 = 9  =>  (4 - s)^2 = 9  =>  s = +1

		=>

		k = sqrt(p2/p1) - 1
		p = first * (kx + 1)^2
		*/

		if (first == 0)
			return -1;

		const double k = sqrt(last / first) - 1.0;
		return _math.round_to_int(first * _math.sqr(k * position + 1.0));
	}

	// Расчет параболически изменяющегося набора периодов по крайним значениям для индикаторов серии Multi*
	static int range_periods_parabolic(int first, int last, int count, int &periods[])
	{
		ArrayResize(periods, count);

		if (first == last)
		{
			ArrayInitialize(periods, first);
			return count;
		}

		const double step = 1.0 / (count - 1);

		for (int i = 0; i < count; i++)
			periods[i] = range_period_parabolic(first, last, i * step);

		return count;
	}

	static int range_period(int first, int last, ENUM_STEP step_type, double position)
	{
		switch (step_type)
		{
			case STEP_LINEAR:     return range_period_linear(first, last, position);
			case STEP_EXP:        return range_period_exponential(first, last, position);
			case STEP_PARABOLIC:  return range_period_parabolic(first, last, position);

			case STEP_PAR_EXP:
				{
					int exp_period = range_period_exponential(first, last, position);
					int par_period = range_period_parabolic(first, last, position);
					if (exp_period < 0 || par_period < 0)
						return 0;

					return _math.round_to_int((exp_period + par_period) / 2.0);
				}

			case STEP_LIN_PAR:
				{
					int lin_period = range_period_linear(first, last, position);
					int par_period = range_period_parabolic(first, last, position);
					if (lin_period < 0 || par_period < 0)
						return 0;

					return _math.round_to_int((lin_period + par_period) / 2.0);
				}
		}

		return 0;
	}

	static int periods(int first, int last, int count, ENUM_STEP step_type, string user_periods, int &periods[])
	{
		if (_str.trim(user_periods) != "")
			return periods_user(user_periods, INT_MAX, periods);

		if ((count <= 0) || (first < 0) || (last < 0))
			return 0;

		if (count == 1)
		{
			ArrayResize(periods, 1);
			return ArrayInitialize(periods, first);
		}

		//if (first == last)
		//	return ArrayAdd(periods, first) + 1;

	//
	//	{
	//		ArrayResize(periods, count);
	//		ArrayInitialize(periods, first);
	//		return count;
	//	}

		switch (step_type)
		{
			case STEP_LINEAR:     return range_periods_linear(first, last, count, periods);
			case STEP_EXP:        return range_periods_exponential(first, last, count, periods);
			case STEP_PARABOLIC:  return range_periods_parabolic(first, last, count, periods);

			case STEP_PAR_EXP:
				{
					int exp_periods[];
					int exp_count = range_periods_exponential(first, last, count, exp_periods);

					int par_periods[];
					int par_count = range_periods_parabolic(first, last, count, par_periods);

					if (exp_count != par_count)
						return 0;

					ArrayResize(periods, exp_count);

					for (int i = 0; i < exp_count; i++)
						periods[i] = _math.round_to_int((double)(exp_periods[i] + par_periods[i]) / 2.0);

					return exp_count;
				}

			case STEP_LIN_PAR:
				{
					int lin_periods[];
					int lin_count = range_periods_linear(first, last, count, lin_periods);

					int par_periods[];
					int par_count = range_periods_parabolic(first, last, count, par_periods);

					if (lin_count != par_count)
						return 0;

					ArrayResize(periods, lin_count);

					for (int i = 0; i < lin_count; i++)
						periods[i] = _math.round_to_int((double)(lin_periods[i] + par_periods[i]) / 2.0);

					return lin_count;
				}
		}

		return 0;
	}

	static string periods_string(const string custom_periods, const int &periods[], const ENUM_STEP step)
	{
		return
			(custom_periods != "")
				? _str.join(periods, ",")
				: _str.format_range(periods, enum_step_to_string(step, true))
			;
	}

} _multi;
