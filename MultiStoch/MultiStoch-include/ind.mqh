/*
Copyright 2023 FXcoder

This file is part of MultiStoch.

MultiStoch is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MultiStoch is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with MultiStoch. If not, see
http://www.gnu.org/licenses/.
*/

// Индикаторные функции. © FXcoder

class CIndicatorUtils
{
public:

	double get_value(int handle, int buffer_num, datetime bar_time)
	{
		if (handle == INVALID_HANDLE)
			return EMPTY_VALUE;

		double values[];
		return CopyBuffer(handle, buffer_num, bar_time, 1, values) == 1 ? values[0] : EMPTY_VALUE;
	}

	static void release(const int &handles[])
	{
		for (int i = ArraySize(handles) - 1; i >= 0; --i)
			IndicatorRelease(handles[i]);
	}

	// -1 on error (bad input)
	static int get_bars_to_calculate(int rates_total, int prev_calculated, int at_least = 0, int at_most = 0, int reserve = 0)
	{
		if ((at_most > 0 && at_most < at_least) ||
			rates_total < 0 || prev_calculated < 0 ||
			at_least < 0 || at_most < 0 || reserve < 0)
		{
			return -1;
		}

		// максимально возможный результат, не приводящий к ошибке в вызываемом коде
		const int max_result = rates_total - reserve;

		if (at_least > max_result)
			return -1;

		// нерассчитанные бары
		int bars_to_calculate = rates_total - prev_calculated;

		if (at_least > 0)
		{
			if (bars_to_calculate < at_least)
				bars_to_calculate = at_least;
		}

		if (bars_to_calculate > max_result)
			bars_to_calculate = max_result;

		if (at_most > 0)
		{
			if (bars_to_calculate > at_most)
				bars_to_calculate = at_most;
		}

		return bars_to_calculate;
	}
} _ind;
