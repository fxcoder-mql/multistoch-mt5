/*
Copyright 2023 FXcoder

This file is part of MultiStoch.

MultiStoch is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MultiStoch is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with MultiStoch. If not, see
http://www.gnu.org/licenses/.
*/

// Параметры градиента. © FXcoder

#include "enum/gradient.mqh"
#include "enum/multi_theme.mqh"

struct CGradientInfo
{
public:

	ENUM_MULTI_THEME theme;
	color start_color;
	color mid_color;
	color end_color;
	ENUM_GRADIENT gradient_type;

	void CGradientInfo():
		theme(MULTI_THEME_CUSTOM),
		start_color(clrRed),
		mid_color(clrLime),
		end_color(clrBlue),
		gradient_type(GRADIENT_RGB)
	{
	}

	void CGradientInfo(
		ENUM_MULTI_THEME theme_,
		color start_color_,
		color mid_color_,
		color end_color_,
		ENUM_GRADIENT gradient_type_
		):
			theme(theme_),
			start_color(start_color_),
			mid_color(mid_color_),
			end_color(end_color_),
			gradient_type(gradient_type_)
	{
	}
};
