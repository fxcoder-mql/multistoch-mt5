/*
Copyright 2023 FXcoder

This file is part of MultiStoch.

MultiStoch is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MultiStoch is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with MultiStoch. If not, see
http://www.gnu.org/licenses/.
*/

// Функции цветовой схемы (темы). © FXcoder

#include "enum/gradient.mqh"
#include "enum/multi_theme.mqh"
#include "gradient_info.mqh"
#include "chart.mqh"
#include "color.mqh"

// Создать градиент цветов на основе трёх точек (начало, центр, конец).
// Если нужен градиент по двум цветам, указать mid_color = clrNONE.
bool color_gradient(color start_color, color mid_color, color end_color, int count, double step, ENUM_GRADIENT gradient_type, color &colors[])
{
	if (count == 0)
		return false;

	if (count == 1)
	{
		ArrayResize(colors, 1);
		colors[0] = start_color;
		return true;
	}

	// Если один из цветов не указан, считать нечего.
	if (_color.is_none(start_color) || _color.is_none(mid_color) || _color.is_none(end_color))
		return false;

	ArrayResize(colors, count);

	const bool is_odd = (count % 2) != 0;
	const int mid = count / 2;
	const double half_len = ((double)count - 1.0) / 2.0;

	// Скорректировать Hue набора так, чтобы он был последовательным
	if (gradient_type == GRADIENT_HSV)
	{
		// По направлению кратчайшего движения от 1 к 2 определить общее направление.
		uint hsv1 = _color.to_hsv(start_color);
		uint hsv2 = _color.to_hsv(mid_color);
		uint hsv3 = _color.to_hsv(end_color);
		const int h1 = _hsv.hue(hsv1);
		const int h2 = _hsv.hue(hsv2);
		const int h3 = _hsv.hue(hsv3);

		const int a21 = (h2 - h1 + 360) % 360;
		const bool forward = a21 <= 180;

		// коррекция для правильного отсчёта (в ближайшую сторону)
		if (forward)
		{
			if (h2 < h1)
				hsv2 = _hsv.shift_hue(hsv2, 360, false);
		}
		else
		{
			if (h2 > h1)
				hsv1 = _hsv.shift_hue(hsv1, 360, false);
		}

		// первая часть между 1 и 2
		for (int i = 0; i < mid; i++)
			colors[i] = _hsv.to_color(_hsv.mix(hsv1, hsv2, 1.0 * i / half_len, step));

		// вернуть второму цвету оттенок в нормальном диапазоне
		hsv2 = _hsv.normalize(hsv2);

		// повторить коррекцию для пары 2-3, направление оставить таким же, как для 1-2
		if (forward)
		{
			if (h3 < h2)
				hsv3 = _hsv.shift_hue(hsv3, 360, false);
		}
		else
		{
			if (h3 > h2)
				hsv2 = _hsv.shift_hue(hsv2, 360, false);
		}

		// вторая часть между 2 и 3
		for (int i = mid; i < count; i++)
			colors[i] = _hsv.to_color(_hsv.mix(hsv2, hsv3, 1.0 * (i - (mid - (is_odd ? 0 : 0.5))) / half_len, step));
	}
	else // if (gradient_type == GRADIENT_RGB)
	{
		// первая часть между 1 и 2
		for (int i = 0; i < mid; i++)
			colors[i] = _color.mix(start_color, mid_color, 1.0 * i / half_len, step);

		// вторая часть между 2 и 3
		for (int i = mid; i < count; i++)
			colors[i] = _color.mix(mid_color, end_color, 1.0 * (i - (mid - (is_odd ? 0 : 0.5))) / half_len, step);
	}

	return true;
}

bool color_gradient(const CGradientInfo &gi, const int count, color &colors[])
{
	return color_gradient(gi.theme, gi.start_color, gi.mid_color, gi.end_color, count, gi.gradient_type, colors);
}

bool color_gradient(const ENUM_MULTI_THEME theme, color start_color, color mid_color, color end_color, const int count, const ENUM_GRADIENT gradient_type, color &colors[])
{
	get_theme_colors(theme, start_color, mid_color, end_color);
	const double step = 8.0;

	if (_color.is_none(mid_color))
		mid_color = get_mid_color(start_color, end_color, step, gradient_type);

	if (enum_gradient_is_reverse(gradient_type))
		return color_gradient(end_color, mid_color, start_color, count, step, gradient_type, colors);
	else
		return color_gradient(start_color, mid_color, end_color, count, step, gradient_type, colors);
}

color get_mid_color(color start_color, color end_color, double step, ENUM_GRADIENT gradient_type)
{
	if (enum_gradient_is_hsv(gradient_type))
		return _hsv.to_color(_hsv.mix(_color.to_hsv(start_color), _color.to_hsv(end_color), 0.5, step));

	// gradient_type == GRADIENT_RGB
	return _color.mix(start_color, end_color, 0.5, step);
}

void get_theme_colors(const ENUM_MULTI_THEME theme, color &start_color, color &mid_color, color &end_color)
{
	if (enum_multi_theme_to_colors(theme, start_color, mid_color, end_color))
		return;

	// Специальные случаи

	if (theme == MULTI_THEME_FG_BG)                           // 1001
	{
		start_color = _chart.color_foreground();
		mid_color   = clrNONE;
		end_color   = _chart.color_background();
	}
	else if (theme == MULTI_THEME_START_INVERSE)              // 1007
	{
		//start_color = start_color;
		mid_color = clrNONE;
		end_color = _color.invert(start_color);
	}
	else if (theme == MULTI_THEME_START_SV_INVERSE)           // 1008
	{
		//start_color = start_color;
		mid_color = clrNONE;
		end_color =	_color.invert_save_hue(start_color);
	}
	else if (theme == MULTI_THEME_START_BGAVG25)              // 1010
	{
		//start_color = start_color;
		mid_color = clrNONE;
		end_color = _color.mix(_chart.color_background(), start_color, 0.25, 1.0);
	}
	else
	{
		start_color = clrNONE;
		mid_color = clrNONE;
		end_color = clrNONE;
	}
}
