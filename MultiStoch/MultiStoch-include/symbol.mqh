/*
Copyright 2023 FXcoder

This file is part of MultiStoch.

MultiStoch is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MultiStoch is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with MultiStoch. If not, see
http://www.gnu.org/licenses/.
*/

// Symbol helper. © FXcoder

#include "sym.mqh"

#define _G_SYMBOL_GET(T, N, P) T N() const { return((T)get(P)); }
#define _G_SYMBOL_SET(T, N, P) CBSymbol *N(T value) { return(set(P, value)); }

class CSymbol
{
private:

	const string name_;
	const bool is_current_;

public:

	void CSymbol():
		name_(_Symbol),
		is_current_(true)
	{
	}

	void CSymbol(string symbol):
		name_(_sym.real(symbol)),
		is_current_(_sym.is_current(symbol))
	{
	}

	string name() const { return name_; }

	bool select(bool select) const { return(SymbolSelect(name_, select)); }

	_G_SYMBOL_GET(bool, exists, SYMBOL_EXIST)
	_G_SYMBOL_GET(bool, selected, SYMBOL_SELECT)

	// selected_only - искать только в выбранных (в обзоре рынка)
	bool exists(bool selected_only) const
	{
		return exists() && (!selected_only || selected());
	}

	bool tick(MqlTick &tick) const { return SymbolInfoTick(name_, tick); }

private:

	double get(ENUM_SYMBOL_INFO_DOUBLE  property_id) const { return SymbolInfoDouble (name_, property_id); }
	long   get(ENUM_SYMBOL_INFO_INTEGER property_id) const { return SymbolInfoInteger(name_, property_id); }
	string get(ENUM_SYMBOL_INFO_STRING  property_id) const { return SymbolInfoString (name_, property_id); }
};

CSymbol _symbol;
